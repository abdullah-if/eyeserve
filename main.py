from datetime import datetime
import json
import csv

from flask import Flask, render_template, request, redirect


app = Flask(__name__)

@app.route("/", methods=["GET","POST"])
def get():
    if request.method == "POST":
        clientdatas = json.loads(request.data)
        serverdatas = {"DateAndTime":str(datetime.now()),"IP":request.remote_addr}
        datas = serverdatas | clientdatas
        with open("test.csv","a+") as i:
            writer = csv.DictWriter(i, fieldnames = datas.keys(), delimiter=",")
            writer.writerow(datas)
    return render_template("index.html")

if __name__ == "__main__":
    app.run(debug=True)
