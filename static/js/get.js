var performance = window.performance || window.mozPerformance || window.msPerformance || window.webkitPerformance || {};

txt += ('/n');
for (var value in performance) {
txt += (value);
}


var canvas;
var txt = new Object();
txt.AppCodeName = navigator.appCodeName;
txt.AppName = navigator.appName;
txt.AppVersion = navigator.appVersion;
txt.Language = navigator.language;
txt.OnLine = navigator.onLine;
txt.Platform = navigator.platform;
txt.UserAgent= navigator.userAgent;
txt.Vendor = navigator.vendor;
txt.HWConcurency = navigator.hardwareConcurrency;
txt.ScreenRes = screen.width + "x" + screen.height;
txt.AvailableRes = screen.availWidth + "x" + screen.availHeight;
txt.ColorDepth = screen.colorDepth;
txt.PixelDepth= screen.pixelDepth;
var d = new Date();
txt.TimezoneOffset = d.getTimezoneOffset()/(-60);



canvas = document.getElementById("myCanvas");
var gl = canvas.getContext("experimental-webgl");

txt.Renderer = gl.getParameter(gl.RENDERER);
txt.MaskedVendor = gl.getParameter(gl.VENDOR);
txt.GPUVendor = getUnmaskedInfo(gl).vendor;
txt.GPURenderer = getUnmaskedInfo(gl).renderer;


function getUnmaskedInfo(gl) {
var unMaskedInfo = {
    renderer: '',
    vendor: ''
};

var dbgRenderInfo = gl.getExtension("WEBGL_debug_renderer_info");
if (dbgRenderInfo != null) {
    unMaskedInfo.renderer = gl.getParameter(dbgRenderInfo.UNMASKED_RENDERER_WEBGL);
    unMaskedInfo.vendor = gl.getParameter(dbgRenderInfo.UNMASKED_VENDOR_WEBGL);
}

return unMaskedInfo;

}
var txtjson = JSON.stringify(txt);
var url = "/";
var http = new XMLHttpRequest();
http.open("POST", url, true);

http.setRequestHeader("Content-type", 'text/plain');
http.send(txtjson);
